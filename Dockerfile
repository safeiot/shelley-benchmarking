FROM python:3.8
WORKDIR /workdir
ARG BENCHMARKTK_HASH=b7c804a9fbc68f3a29a0dd4404b037e24ff5d0d2
RUN git clone https://gitlab.com/cogumbreiro/benchmarktk && \
    cd benchmarktk && \
    git reset --hard ${BENCHMARKTK_HASH}
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY src src/
COPY Makefile .
ENV PATH=$PATH:/workdir/benchmarktk
