DOCKER_COMPOSE=docker-compose

all: build

build:
	$(MAKE) -C src

run:
	mkdir -p logs/combiningvalves logs/numberofvalves
	$(MAKE) -C src run

clean:
	$(MAKE) -C src clean

docker:
	$(DOCKER_COMPOSE) build

docker-shell:
	$(DOCKER_COMPOSE) run main


.PHONY: all run clean docker build