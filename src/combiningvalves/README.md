# Several valves integration

## Run alternative #1

### Setup

	```
	make build1
	```

### Run

log options:
	4,
	8,
	12,
	16,
	24,
	32

	```
	shelleyc -d {count}valves.yml -u valve.scy:Valve --no-output --dump-timings {log}
	```

## Run alternative #2


### Setup

	```
	make build2

	```

### Run

log options:
	2x4,
	3x4,
	8x4

	```
	shelleyc -d {count}valves.yml -u 4valves.scy:ValveHandler --no-output --dump-timings {log}
	```


## Run alternative #3


### Setup

	```
	make build3

	```

### Run

log options:
	4x2x4

	```
	shelleyc -d {count}valves.yml -u 2x4valves.scy:SuperValveHandler --no-output --dump-timings {log}
	```

